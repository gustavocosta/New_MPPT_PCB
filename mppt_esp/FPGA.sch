EESchema Schematic File Version 2
LIBS:mppt_microchip-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:40xx
LIBS:74xgxx
LIBS:5050_ws2811
LIBS:a4988-module
LIBS:ac-dc
LIBS:acs712
LIBS:actel
LIBS:adxl345
LIBS:Altera
LIBS:AMS1117
LIBS:analog_devices
LIBS:analog_ICs_PL
LIBS:ant
LIBS:arduinoshield
LIBS:at24c64
LIBS:bc56-12EWA
LIBS:brooktre
LIBS:cis
LIBS:cmos_ieee
LIBS:con-kycon
LIBS:cp2103
LIBS:CP2102
LIBS:dc-dc
LIBS:dht11
LIBS:diode
LIBS:displays_PL
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:esp8266-module
LIBS:ESP8266
LIBS:fe
LIBS:ftdi
LIBS:Gajda_opto
LIBS:gennum
LIBS:graphic
LIBS:GS6300
LIBS:gy-a4988_module
LIBS:hc11
LIBS:hc541
LIBS:hc-05
LIBS:HC164
LIBS:hlk-rm04
LIBS:ir
LIBS:irf7907
LIBS:isp_6p
LIBS:kicad-library-by-simlun
LIBS:l298n
LIBS:Lattice
LIBS:LM2575
LIBS:logo
LIBS:logo-azura-wrench
LIBS:M25P05
LIBS:max1538
LIBS:maxim
LIBS:mcp1640
LIBS:mcp1640b
LIBS:mcp4921
LIBS:MCP3208
LIBS:microchip1
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers_PL
LIBS:Mimas-cache
LIBS:Mimas-rescue
LIBS:miniusb
LIBS:motor_drivers
LIBS:mp2109dq
LIBS:MP2307DN
LIBS:mppt-cache
LIBS:msp430
LIBS:na10-psu
LIBS:noname-cache
LIBS:nordicsemi
LIBS:nrf24l01
LIBS:numato_kicad_lib
LIBS:nxp_armmcu
LIBS:OF5032
LIBS:onsemi
LIBS:opamps
LIBS:opamps_PL
LIBS:Oscillators
LIBS:pl2303
LIBS:PL_capacitors
LIBS:PL_connectors
LIBS:PL_diodes
LIBS:PL_inductors
LIBS:PL_LEDs
LIBS:PL_mounting_holes
LIBS:PL_opto
LIBS:PL_switchers
LIBS:PL_switches
LIBS:PL_transistors
LIBS:pogopin
LIBS:Pot
LIBS:powerint
LIBS:powerjack
LIBS:Power_Management
LIBS:pspice
LIBS:pt4115
LIBS:references
LIBS:references_PL
LIBS:regulators_PL
LIBS:relays
LIBS:relays_PL
LIBS:rfcom
LIBS:RJ45-ENCODER
LIBS:rtl8196
LIBS:sensors
LIBS:silabs
LIBS:songle_relay
LIBS:special
LIBS:spiflash_16m
LIBS:st1s10
LIBS:stc12c5a60s2
LIBS:stepper_drivers
LIBS:stm8
LIBS:stm32
LIBS:STM32F100RBT6B
LIBS:stpdn
LIBS:supertex
LIBS:switches
LIBS:switch-omron
LIBS:SymbolsSimilarEN60617+oldDIN617
LIBS:tcs3200d
LIBS:tda5144
LIBS:texas-msp
LIBS:tp4056
LIBS:transf
LIBS:ts812c
LIBS:ttl_ieee
LIBS:usb_a
LIBS:usb-a-pcb
LIBS:usb_ax2
LIBS:video
LIBS:vref_PL
LIBS:w_analog
LIBS:w_connectors
LIBS:w_device
LIBS:w_logic
LIBS:Worldsemi
LIBS:xc3s100etq144
LIBS:xc3s500evq100
LIBS:xc6slx9-tqg144
LIBS:XC9572-TQ100
LIBS:Xicor
LIBS:xilinx_spartan3_virtex4_and_5
LIBS:Zilog
LIBS:рэс-49
LIBS:тпг-1
LIBS:KICAD-OPEN-LIBS
LIBS:4000-ic
LIBS:7400-ic
LIBS:analog-ic
LIBS:avr-mcu
LIBS:bluegiga
LIBS:connector
LIBS:diode-inc-ic
LIBS:freescale-ic
LIBS:ftdi-ic
LIBS:led
LIBS:maxim-ic
LIBS:micrel-ic
LIBS:microchip-ic
LIBS:nxp-ic
LIBS:on-semi-ic
LIBS:regulator
LIBS:rohm
LIBS:sharp-relay
LIBS:sparkfun
LIBS:standard
LIBS:stmicro-mcu
LIBS:ti-ic
LIBS:transistor
LIBS:uln-ic
LIBS:74LVC1G04GW
LIBS:93c56
LIBS:93CXX
LIBS:amesser-ad
LIBS:amesser-cmos4000
LIBS:amesser-conn
LIBS:amesser-discrete
LIBS:amesser-linear
LIBS:amesser-power
LIBS:amesser-usb
LIBS:audio-vlsi
LIBS:bat54_c
LIBS:bav199
LIBS:BLM15HG6015N1D
LIBS:bss138dp
LIBS:CIAA_ACC
LIBS:CIAAK60LIB
LIBS:DDR3_x16
LIBS:edu-ciaa-fsl
LIBS:edu-ciaa-nxp
LIBS:EduCiaaXSchLibrary
LIBS:FMC_HPC
LIBS:fqt13n06l
LIBS:ft232bm
LIBS:ft232rq
LIBS:ft2232h
LIBS:FT2232H
LIBS:g5v2
LIBS:intel_edison
LIBS:IS42S16400F
LIBS:lan_8740
LIBS:led_arbg
LIBS:lm2596
LIBS:lpc54102j512bd64
LIBS:LPC4337JBD144
LIBS:mcp1415
LIBS:mcp1416
LIBS:mcu-nxp
LIBS:mcu-st
LIBS:mic2039eymt
LIBS:MIC2025
LIBS:microusb
LIBS:net-phy
LIBS:nsi50010yt1g
LIBS:pbss5240xf
LIBS:PBSS5240
LIBS:pcie-mini
LIBS:Pic32MZ
LIBS:prtr5v0u2x
LIBS:PRTR5V0U2X
LIBS:PS2805
LIBS:regulators
LIBS:rs485_rs232_can
LIBS:r_small
LIBS:rx63n_lqfp144
LIBS:s25fl032p0xmfi013
LIBS:SCHA5B0200
LIBS:sd-con
LIBS:shield_arduino
LIBS:Si50x
LIBS:SMD_Sealing_Glass_Crystal
LIBS:sw_push
LIBS:sw_push4
LIBS:tblocks
LIBS:TJA1040
LIBS:TPS2051B
LIBS:transf_unip
LIBS:Transil_diode
LIBS:tvs
LIBS:TXB0108
LIBS:txs0206-29
LIBS:XC7Z030_FBG676
LIBS:XilinxArtix7
LIBS:mppt_microchip-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 4900 2100 0    60   ~ 0
GPIO-P1
Text Label 4900 2200 0    60   ~ 0
GPIO-P2
Text Label 4900 2300 0    60   ~ 0
GPIO-P3
Text Label 4900 2400 0    60   ~ 0
GPIO-P4
Text Label 4900 2500 0    60   ~ 0
GPIO-P5
Text Label 4900 2600 0    60   ~ 0
GPIO-P6
Text Label 4900 2700 0    60   ~ 0
GPIO-P7
Text Label 6700 2200 2    60   ~ 0
GPIO-N2
Text Label 6700 2300 2    60   ~ 0
GPIO-N3
Text Label 6700 2400 2    60   ~ 0
GPIO-N4
Text Label 6700 2500 2    60   ~ 0
GPIO-N5
Text Label 6700 2600 2    60   ~ 0
GPIO-N6
Text Label 6700 2700 2    60   ~ 0
GPIO-N7
Text Label 4900 2800 0    60   ~ 0
GPIO-P8
Text Label 4900 2900 0    60   ~ 0
GPIO-P9
Text Label 4900 3000 0    60   ~ 0
GPIO-P10
Text Label 4900 3100 0    60   ~ 0
GPIO-P11
Text Label 4900 3200 0    60   ~ 0
GPIO-P12
Text Label 4900 3300 0    60   ~ 0
GPIO-P13
Text Label 4900 3400 0    60   ~ 0
GPIO-P14
Text Label 6700 2800 2    60   ~ 0
GPIO-N8
Text Label 6700 2900 2    60   ~ 0
GPIO-N9
Text Label 6700 3000 2    60   ~ 0
GPIO-N10
Text Label 6700 3100 2    60   ~ 0
GPIO-N11
Text Label 6700 3200 2    60   ~ 0
GPIO-N12
Text Label 6700 3300 2    60   ~ 0
GPIO-N13
Text Label 6700 3400 2    60   ~ 0
GPIO-N14
Text Label 4900 5000 0    60   ~ 0
GND
Text Label 4900 3500 0    60   ~ 0
GPIO-P15
Text Label 4900 3600 0    60   ~ 0
GPIO-P16
Text Label 4900 3700 0    60   ~ 0
GPIO-P17
Text Label 4900 3800 0    60   ~ 0
GPIO-P18
Text Label 4900 5400 0    60   ~ 0
GPIO-P19
Text Label 6700 3500 2    60   ~ 0
GPIO-N15
Text Label 6700 3600 2    60   ~ 0
GPIO-N16
Text Label 6700 3700 2    60   ~ 0
GPIO-N17
Text Label 6700 3800 2    60   ~ 0
GPIO-N18
Text Label 6700 5400 2    60   ~ 0
GPIO-N19
Text Label 4900 5500 0    60   ~ 0
GPIO-P22
Text Label 4900 5600 0    60   ~ 0
GPIO-P24
Text Label 4900 5300 0    60   ~ 0
GPIO-P26
Text Label 4900 5200 0    60   ~ 0
GPIO-P27
Text Label 4900 5100 0    60   ~ 0
GPIO-P28
Text Label 6700 5500 2    60   ~ 0
GPIO-N22
Text Label 6700 5600 2    60   ~ 0
GPIO-N24
Text Label 6700 5300 2    60   ~ 0
GPIO-N26
Text Label 6700 5200 2    60   ~ 0
GPIO-N27
Text Label 4900 4900 0    60   ~ 0
GPIO-P29
Text Label 4900 4800 0    60   ~ 0
GPIO-P30
Text Label 4900 4700 0    60   ~ 0
GPIO-P31
Text Label 4900 4600 0    60   ~ 0
GPIO-P32
Text Label 4900 4500 0    60   ~ 0
GPIO-P33
Text Label 4900 4400 0    60   ~ 0
GPIO-P34
Text Label 4900 4300 0    60   ~ 0
GPIO-P35
Text Label 6700 4900 2    60   ~ 0
GPIO-N29
Text Label 6700 4800 2    60   ~ 0
GPIO-N30
Text Label 6700 4700 2    60   ~ 0
GPIO-N31
Text Label 6700 4600 2    60   ~ 0
GPIO-N32
Text Label 6700 4500 2    60   ~ 0
GPIO-N33
Text Label 6700 4400 2    60   ~ 0
GPIO-N34
Text Label 6700 4300 2    60   ~ 0
GPIO-N35
Text Label 4900 4200 0    60   ~ 0
GPIO-P20
Text Label 4900 4100 0    60   ~ 0
GPIO-P21
Text Label 4900 5800 0    60   ~ 0
GPIO-P23
Text Label 6700 4200 2    60   ~ 0
GPIO-N20
Text Label 6700 4100 2    60   ~ 0
GPIO-N21
Text Label 6700 5800 2    60   ~ 0
GPIO-N23
Text Label 4900 5900 0    60   ~ 0
VCCIO
Text Label 4900 2000 0    60   ~ 0
VCCIO
Text Label 4900 4000 0    60   ~ 0
VCCIO
$Comp
L CONN_20X2 P6
U 1 1 5A1C72AA
P 5800 2950
F 0 "P6" H 5800 3050 60  0000 C CNN
F 1 "CONN_20X2" V 5800 2950 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x20_Pitch2.54mm" H 5800 2950 60  0001 C CNN
F 3 "" H 5800 2950 60  0000 C CNN
	1    5800 2950
	-1   0    0    -1  
$EndComp
$Comp
L CONN_20X2 P7
U 1 1 5A1C72B1
P 5800 4950
F 0 "P7" H 5800 4950 60  0000 C CNN
F 1 "CONN_20X2" V 5800 4950 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x20_Pitch2.54mm" H 5800 4950 60  0001 C CNN
F 3 "" H 5800 4950 60  0000 C CNN
	1    5800 4950
	1    0    0    -1  
$EndComp
Text Label 4900 5700 0    60   ~ 0
GPIO-P25
Text Label 6700 5700 2    60   ~ 0
GPIO-N25
Text Label 6700 5000 2    60   ~ 0
GND
Text Label 4900 3900 0    60   ~ 0
VCCIO
$Comp
L R R15
U 1 1 5A1C72CC
P 4650 2200
F 0 "R15" V 4730 2200 50  0000 C CNN
F 1 "330" V 4650 2200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 4580 2200 50  0001 C CNN
F 3 "" H 4650 2200 50  0000 C CNN
	1    4650 2200
	0    1    1    0   
$EndComp
$Comp
L LED-RESCUE-mppt_microchip D1
U 1 1 5A1C72D3
P 4200 2200
F 0 "D1" H 4200 2300 50  0000 C CNN
F 1 "LED" H 4200 2100 50  0000 C CNN
F 2 "LEDs:LED_0805" H 4200 2200 50  0001 C CNN
F 3 "" H 4200 2200 50  0000 C CNN
	1    4200 2200
	1    0    0    -1  
$EndComp
NoConn ~ 6700 5800
NoConn ~ 6700 5700
NoConn ~ 6700 5600
NoConn ~ 6700 5500
NoConn ~ 6700 5400
NoConn ~ 6700 5300
NoConn ~ 6700 5200
NoConn ~ 4900 5800
NoConn ~ 4900 5700
NoConn ~ 4900 5600
NoConn ~ 4900 5200
NoConn ~ 4900 5300
NoConn ~ 4900 5400
NoConn ~ 4900 5500
Text Label 6700 2100 3    60   ~ 0
GPIO-N1
NoConn ~ 6700 2900
NoConn ~ 4900 2900
NoConn ~ 4900 3000
NoConn ~ 4900 4800
NoConn ~ 4900 4900
NoConn ~ 6700 4800
NoConn ~ 6700 4900
NoConn ~ 4900 5100
NoConn ~ 6700 2800
NoConn ~ 6700 2700
NoConn ~ 6700 2600
NoConn ~ 6700 2500
NoConn ~ 6700 2400
NoConn ~ 6700 2300
NoConn ~ 6700 2200
NoConn ~ 6700 2100
NoConn ~ 4900 2300
NoConn ~ 4900 2400
NoConn ~ 4900 2100
NoConn ~ 4900 2500
NoConn ~ 4900 2600
NoConn ~ 4900 2700
NoConn ~ 4900 2800
NoConn ~ 4900 3100
Wire Wire Line
	6200 2700 6700 2700
Wire Wire Line
	6200 2600 6700 2600
Wire Wire Line
	6200 2900 6700 2900
Wire Wire Line
	6200 3200 6700 3200
Wire Wire Line
	6200 3100 6700 3100
Wire Wire Line
	6200 3000 6700 3000
Wire Wire Line
	5400 3400 4900 3400
Wire Wire Line
	5400 2900 4900 2900
Wire Wire Line
	5400 3000 4900 3000
Wire Wire Line
	5400 3100 4900 3100
Wire Wire Line
	5400 3200 4900 3200
Wire Wire Line
	5400 3300 4900 3300
Wire Wire Line
	6200 5900 6700 5900
Wire Wire Line
	6200 4100 6700 4100
Wire Wire Line
	6200 3700 6700 3700
Wire Wire Line
	5400 3500 4900 3500
Wire Wire Line
	5400 3600 4900 3600
Wire Wire Line
	5400 3700 4900 3700
Wire Wire Line
	6200 4500 6700 4500
Wire Wire Line
	6200 4800 6700 4800
Wire Wire Line
	6200 4700 6700 4700
Wire Wire Line
	6200 4600 6700 4600
Wire Wire Line
	5400 5900 4900 5900
Wire Wire Line
	5400 4800 4900 4800
Wire Wire Line
	6200 5100 7850 5100
Wire Wire Line
	6200 5200 6700 5200
Wire Wire Line
	6200 5700 6700 5700
Wire Wire Line
	6200 5600 6700 5600
Wire Wire Line
	6200 5500 6700 5500
Wire Wire Line
	6200 5400 6700 5400
Wire Wire Line
	6200 5300 6700 5300
Wire Wire Line
	5400 5700 4900 5700
Wire Wire Line
	5400 5100 4900 5100
Wire Wire Line
	5400 5200 4900 5200
Wire Wire Line
	5400 5300 4900 5300
Wire Wire Line
	5400 5400 4900 5400
Wire Wire Line
	5400 5500 4900 5500
Wire Wire Line
	5400 5600 4900 5600
Wire Wire Line
	6200 4300 6700 4300
Wire Wire Line
	6200 4400 6700 4400
Wire Wire Line
	5400 3800 4900 3800
Wire Wire Line
	6200 4200 6700 4200
Wire Wire Line
	5400 4900 4900 4900
Wire Wire Line
	6200 4900 6700 4900
Wire Wire Line
	6200 5800 6700 5800
Wire Wire Line
	6200 2500 6700 2500
Wire Wire Line
	6200 2800 6700 2800
Wire Wire Line
	6200 2300 6700 2300
Wire Wire Line
	6200 2200 6700 2200
Wire Wire Line
	6200 4000 7250 4000
Wire Wire Line
	6700 5000 6200 5000
Wire Wire Line
	5400 5000 4900 5000
Wire Wire Line
	4900 5800 5400 5800
Wire Wire Line
	4900 4700 5400 4700
Wire Wire Line
	5400 4600 4900 4600
Wire Wire Line
	4900 4500 5400 4500
Wire Wire Line
	5400 4400 4900 4400
Wire Wire Line
	4900 4300 5400 4300
Wire Wire Line
	5400 4200 4900 4200
Wire Wire Line
	4900 4100 5400 4100
Wire Wire Line
	4300 4000 5400 4000
Wire Wire Line
	4000 3900 5400 3900
Wire Wire Line
	6200 3900 7000 3900
Wire Wire Line
	6700 3800 6200 3800
Wire Wire Line
	6200 2400 6700 2400
Wire Wire Line
	4900 2100 5400 2100
Wire Wire Line
	4800 2200 5400 2200
Wire Wire Line
	4900 2300 5400 2300
Wire Wire Line
	5400 2400 4900 2400
Wire Wire Line
	4900 2500 5400 2500
Wire Wire Line
	5400 2600 4900 2600
Wire Wire Line
	4900 2700 5400 2700
Wire Wire Line
	5400 2800 4900 2800
Wire Wire Line
	6700 3300 6200 3300
Wire Wire Line
	6200 3400 6700 3400
Wire Wire Line
	6700 3500 6200 3500
Wire Wire Line
	6200 3600 6700 3600
Wire Wire Line
	4500 2200 4400 2200
Wire Wire Line
	4000 2200 3900 2200
Wire Wire Line
	3900 2200 3900 2300
Wire Wire Line
	6200 2100 6700 2100
Text Label 6700 5100 2    60   ~ 0
GPIO-N28
$Comp
L SW_PUSH SW3
U 1 1 5A1C7360
P 7600 5600
F 0 "SW3" H 7750 5710 50  0000 C CNN
F 1 "SW_PUSH" H 7600 5520 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 7600 5600 50  0001 C CNN
F 3 "" H 7600 5600 50  0000 C CNN
	1    7600 5600
	0    -1   -1   0   
$EndComp
$Comp
L R R18
U 1 1 5A1C7367
P 8000 5100
F 0 "R18" V 8080 5100 50  0000 C CNN
F 1 "10k" V 8000 5100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 7930 5100 50  0001 C CNN
F 3 "" H 8000 5100 50  0000 C CNN
	1    8000 5100
	0    1    1    0   
$EndComp
Connection ~ 7600 5100
NoConn ~ 4900 4700
Wire Wire Line
	7600 5100 7600 5300
Wire Wire Line
	7600 5900 7600 6000
$Comp
L C C18
U 1 1 5A1C7372
P 8000 5600
F 0 "C18" H 8025 5700 50  0000 L CNN
F 1 "100nF" H 8025 5500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 8038 5450 50  0001 C CNN
F 3 "" H 8000 5600 50  0001 C CNN
	1    8000 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 5450 8000 5250
Wire Wire Line
	8000 5250 7600 5250
Connection ~ 7600 5250
Wire Wire Line
	8000 5750 8000 5950
Wire Wire Line
	8000 5950 7600 5950
Connection ~ 7600 5950
$Comp
L C C17
U 1 1 5A1C737F
P 3850 3900
F 0 "C17" H 3875 4000 50  0000 L CNN
F 1 "100nF" H 3875 3800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3888 3750 50  0001 C CNN
F 3 "" H 3850 3900 50  0001 C CNN
	1    3850 3900
	0    1    1    0   
$EndComp
$Comp
L GND #PWR040
U 1 1 5A1C7386
P 3900 2300
F 0 "#PWR040" H 3900 2050 50  0001 C CNN
F 1 "GND" H 3900 2150 50  0000 C CNN
F 2 "" H 3900 2300 50  0001 C CNN
F 3 "" H 3900 2300 50  0001 C CNN
	1    3900 2300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR041
U 1 1 5A1C738C
P 7600 6000
F 0 "#PWR041" H 7600 5750 50  0001 C CNN
F 1 "GND" H 7600 5850 50  0000 C CNN
F 2 "" H 7600 6000 50  0001 C CNN
F 3 "" H 7600 6000 50  0001 C CNN
	1    7600 6000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR042
U 1 1 5A1C7392
P 3600 4100
F 0 "#PWR042" H 3600 3850 50  0001 C CNN
F 1 "GND" H 3600 3950 50  0000 C CNN
F 2 "" H 3600 4100 50  0001 C CNN
F 3 "" H 3600 4100 50  0001 C CNN
	1    3600 4100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR043
U 1 1 5A1C7398
P 6700 2000
F 0 "#PWR043" H 6700 1750 50  0001 C CNN
F 1 "GND" H 6700 1850 50  0000 C CNN
F 2 "" H 6700 2000 50  0001 C CNN
F 3 "" H 6700 2000 50  0001 C CNN
	1    6700 2000
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR044
U 1 1 5A1C739E
P 6700 5900
F 0 "#PWR044" H 6700 5650 50  0001 C CNN
F 1 "GND" H 6700 5750 50  0000 C CNN
F 2 "" H 6700 5900 50  0001 C CNN
F 3 "" H 6700 5900 50  0001 C CNN
	1    6700 5900
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR045
U 1 1 5A1C73A4
P 7000 3900
F 0 "#PWR045" H 7000 3650 50  0001 C CNN
F 1 "GND" H 7000 3750 50  0000 C CNN
F 2 "" H 7000 3900 50  0001 C CNN
F 3 "" H 7000 3900 50  0001 C CNN
	1    7000 3900
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR046
U 1 1 5A1C73AA
P 7250 4000
F 0 "#PWR046" H 7250 3750 50  0001 C CNN
F 1 "GND" H 7250 3850 50  0000 C CNN
F 2 "" H 7250 4000 50  0001 C CNN
F 3 "" H 7250 4000 50  0001 C CNN
	1    7250 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3700 3900 3600 3900
Wire Wire Line
	3600 3900 3600 4100
$Comp
L PWR_FLAG #FLG047
U 1 1 5A1C73B2
P 5300 2000
F 0 "#FLG047" H 5300 2075 50  0001 C CNN
F 1 "PWR_FLAG" H 5300 2150 50  0000 C CNN
F 2 "" H 5300 2000 50  0001 C CNN
F 3 "" H 5300 2000 50  0001 C CNN
	1    5300 2000
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR048
U 1 1 5A1C73B8
P 4900 2000
F 0 "#PWR048" H 4900 1850 50  0001 C CNN
F 1 "+3.3V" H 4900 2140 50  0000 C CNN
F 2 "" H 4900 2000 50  0001 C CNN
F 3 "" H 4900 2000 50  0001 C CNN
	1    4900 2000
	0    -1   -1   0   
$EndComp
Connection ~ 5300 2000
Wire Wire Line
	4900 2000 5400 2000
$Comp
L +3.3V #PWR049
U 1 1 5A1C73C0
P 4300 4000
F 0 "#PWR049" H 4300 3850 50  0001 C CNN
F 1 "+3.3V" H 4300 4140 50  0000 C CNN
F 2 "" H 4300 4000 50  0001 C CNN
F 3 "" H 4300 4000 50  0001 C CNN
	1    4300 4000
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR050
U 1 1 5A1C73C6
P 4900 5900
F 0 "#PWR050" H 4900 5750 50  0001 C CNN
F 1 "+3.3V" H 4900 6040 50  0000 C CNN
F 2 "" H 4900 5900 50  0001 C CNN
F 3 "" H 4900 5900 50  0001 C CNN
	1    4900 5900
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR051
U 1 1 5A1C73CC
P 8300 5100
F 0 "#PWR051" H 8300 4950 50  0001 C CNN
F 1 "+3.3V" H 8300 5240 50  0000 C CNN
F 2 "" H 8300 5100 50  0001 C CNN
F 3 "" H 8300 5100 50  0001 C CNN
	1    8300 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	8150 5100 8300 5100
Text HLabel 6700 3100 2    60   Input ~ 0
DB7
Text HLabel 6700 3200 2    60   Input ~ 0
DB6
Text HLabel 6700 3300 2    60   Input ~ 0
DB4
Text HLabel 6700 3400 2    60   Input ~ 0
DB2
Text HLabel 6700 3500 2    60   Input ~ 0
DB0
Text HLabel 6700 3600 2    60   Input ~ 0
CS
Text HLabel 6700 3700 2    60   Input ~ 0
EOC
Text HLabel 6700 3800 2    60   Input ~ 0
A1
Text HLabel 6700 3000 2    60   Input ~ 0
NCO
Text HLabel 4900 3200 0    60   Input ~ 0
DB5
Text HLabel 4900 3300 0    60   Input ~ 0
DB3
Text HLabel 4900 3400 0    60   Input ~ 0
DB1
Text HLabel 4900 3500 0    60   Input ~ 0
CONVST
Text HLabel 4900 3600 0    60   Input ~ 0
RD
Text HLabel 4900 3700 0    60   Input ~ 0
A2
Text HLabel 4900 3800 0    60   Input ~ 0
A0
Text Notes 4500 1650 0    60   ~ 0
Módulo que compreende os pinos que irão encaixar no FPGA\nP6 é o header superior, mais externo na placa\nP7 é o header inferior, mais centralizado na placa
Text Notes 8400 5600 0    60   ~ 0
Botão que inicia a execução do programa
NoConn ~ 4900 4100
NoConn ~ 4900 4200
NoConn ~ 4900 4300
NoConn ~ 4900 4400
NoConn ~ 4900 4500
NoConn ~ 4900 4600
NoConn ~ 6700 4100
NoConn ~ 6700 4200
NoConn ~ 6700 4300
NoConn ~ 6700 4400
NoConn ~ 6700 4500
NoConn ~ 6700 4600
NoConn ~ 6700 4700
$Comp
L PWR_FLAG #FLG052
U 1 1 5AC9B0A3
P 6450 2000
F 0 "#FLG052" H 6450 2095 50  0001 C CNN
F 1 "PWR_FLAG" H 6450 2180 50  0000 C CNN
F 2 "" H 6450 2000 50  0000 C CNN
F 3 "" H 6450 2000 50  0000 C CNN
	1    6450 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2000 6700 2000
Connection ~ 6450 2000
$EndSCHEMATC
