EESchema Schematic File Version 2
LIBS:mppt_microchip-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:40xx
LIBS:74xgxx
LIBS:5050_ws2811
LIBS:a4988-module
LIBS:ac-dc
LIBS:acs712
LIBS:actel
LIBS:adxl345
LIBS:Altera
LIBS:AMS1117
LIBS:analog_devices
LIBS:analog_ICs_PL
LIBS:ant
LIBS:arduinoshield
LIBS:at24c64
LIBS:bc56-12EWA
LIBS:brooktre
LIBS:cis
LIBS:cmos_ieee
LIBS:con-kycon
LIBS:cp2103
LIBS:CP2102
LIBS:dc-dc
LIBS:dht11
LIBS:diode
LIBS:displays_PL
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:esp8266-module
LIBS:ESP8266
LIBS:fe
LIBS:ftdi
LIBS:Gajda_opto
LIBS:gennum
LIBS:graphic
LIBS:GS6300
LIBS:gy-a4988_module
LIBS:hc11
LIBS:hc541
LIBS:hc-05
LIBS:HC164
LIBS:hlk-rm04
LIBS:ir
LIBS:irf7907
LIBS:isp_6p
LIBS:kicad-library-by-simlun
LIBS:l298n
LIBS:Lattice
LIBS:LM2575
LIBS:logo
LIBS:logo-azura-wrench
LIBS:M25P05
LIBS:max1538
LIBS:maxim
LIBS:mcp1640
LIBS:mcp1640b
LIBS:mcp4921
LIBS:MCP3208
LIBS:microchip1
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers_PL
LIBS:Mimas-cache
LIBS:Mimas-rescue
LIBS:miniusb
LIBS:motor_drivers
LIBS:mp2109dq
LIBS:MP2307DN
LIBS:mppt-cache
LIBS:msp430
LIBS:na10-psu
LIBS:noname-cache
LIBS:nordicsemi
LIBS:nrf24l01
LIBS:numato_kicad_lib
LIBS:nxp_armmcu
LIBS:OF5032
LIBS:onsemi
LIBS:opamps
LIBS:opamps_PL
LIBS:Oscillators
LIBS:pl2303
LIBS:PL_capacitors
LIBS:PL_connectors
LIBS:PL_diodes
LIBS:PL_inductors
LIBS:PL_LEDs
LIBS:PL_mounting_holes
LIBS:PL_opto
LIBS:PL_switchers
LIBS:PL_switches
LIBS:PL_transistors
LIBS:pogopin
LIBS:Pot
LIBS:powerint
LIBS:powerjack
LIBS:Power_Management
LIBS:pspice
LIBS:pt4115
LIBS:references
LIBS:references_PL
LIBS:regulators_PL
LIBS:relays
LIBS:relays_PL
LIBS:rfcom
LIBS:RJ45-ENCODER
LIBS:rtl8196
LIBS:sensors
LIBS:silabs
LIBS:songle_relay
LIBS:special
LIBS:spiflash_16m
LIBS:st1s10
LIBS:stc12c5a60s2
LIBS:stepper_drivers
LIBS:stm8
LIBS:stm32
LIBS:STM32F100RBT6B
LIBS:stpdn
LIBS:supertex
LIBS:switches
LIBS:switch-omron
LIBS:SymbolsSimilarEN60617+oldDIN617
LIBS:tcs3200d
LIBS:tda5144
LIBS:texas-msp
LIBS:tp4056
LIBS:transf
LIBS:ts812c
LIBS:ttl_ieee
LIBS:usb_a
LIBS:usb-a-pcb
LIBS:usb_ax2
LIBS:video
LIBS:vref_PL
LIBS:w_analog
LIBS:w_connectors
LIBS:w_device
LIBS:w_logic
LIBS:Worldsemi
LIBS:xc3s100etq144
LIBS:xc3s500evq100
LIBS:xc6slx9-tqg144
LIBS:XC9572-TQ100
LIBS:Xicor
LIBS:xilinx_spartan3_virtex4_and_5
LIBS:Zilog
LIBS:рэс-49
LIBS:тпг-1
LIBS:KICAD-OPEN-LIBS
LIBS:4000-ic
LIBS:7400-ic
LIBS:analog-ic
LIBS:avr-mcu
LIBS:bluegiga
LIBS:connector
LIBS:diode-inc-ic
LIBS:freescale-ic
LIBS:ftdi-ic
LIBS:led
LIBS:maxim-ic
LIBS:micrel-ic
LIBS:microchip-ic
LIBS:nxp-ic
LIBS:on-semi-ic
LIBS:regulator
LIBS:rohm
LIBS:sharp-relay
LIBS:sparkfun
LIBS:standard
LIBS:stmicro-mcu
LIBS:ti-ic
LIBS:transistor
LIBS:uln-ic
LIBS:74LVC1G04GW
LIBS:93c56
LIBS:93CXX
LIBS:amesser-ad
LIBS:amesser-cmos4000
LIBS:amesser-conn
LIBS:amesser-discrete
LIBS:amesser-linear
LIBS:amesser-power
LIBS:amesser-usb
LIBS:audio-vlsi
LIBS:bat54_c
LIBS:bav199
LIBS:BLM15HG6015N1D
LIBS:bss138dp
LIBS:CIAA_ACC
LIBS:CIAAK60LIB
LIBS:DDR3_x16
LIBS:edu-ciaa-fsl
LIBS:edu-ciaa-nxp
LIBS:EduCiaaXSchLibrary
LIBS:FMC_HPC
LIBS:fqt13n06l
LIBS:ft232bm
LIBS:ft232rq
LIBS:ft2232h
LIBS:FT2232H
LIBS:g5v2
LIBS:intel_edison
LIBS:IS42S16400F
LIBS:lan_8740
LIBS:led_arbg
LIBS:lm2596
LIBS:lpc54102j512bd64
LIBS:LPC4337JBD144
LIBS:mcp1415
LIBS:mcp1416
LIBS:mcu-nxp
LIBS:mcu-st
LIBS:mic2039eymt
LIBS:MIC2025
LIBS:microusb
LIBS:net-phy
LIBS:nsi50010yt1g
LIBS:pbss5240xf
LIBS:PBSS5240
LIBS:pcie-mini
LIBS:Pic32MZ
LIBS:prtr5v0u2x
LIBS:PRTR5V0U2X
LIBS:PS2805
LIBS:regulators
LIBS:rs485_rs232_can
LIBS:r_small
LIBS:rx63n_lqfp144
LIBS:s25fl032p0xmfi013
LIBS:SCHA5B0200
LIBS:sd-con
LIBS:shield_arduino
LIBS:Si50x
LIBS:SMD_Sealing_Glass_Crystal
LIBS:sw_push
LIBS:sw_push4
LIBS:tblocks
LIBS:TJA1040
LIBS:TPS2051B
LIBS:transf_unip
LIBS:Transil_diode
LIBS:tvs
LIBS:TXB0108
LIBS:txs0206-29
LIBS:XC7Z030_FBG676
LIBS:XilinxArtix7
LIBS:mppt_microchip-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R11
U 1 1 5A1C7B99
P 6850 3950
F 0 "R11" V 6930 3950 50  0000 C CNN
F 1 "10k" V 6850 3950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6780 3950 50  0001 C CNN
F 3 "" H 6850 3950 50  0000 C CNN
	1    6850 3950
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 5A1C7BD3
P 6750 3950
F 0 "R12" V 6830 3950 50  0000 C CNN
F 1 "10k" V 6750 3950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 3950 50  0001 C CNN
F 3 "" H 6750 3950 50  0000 C CNN
	1    6750 3950
	-1   0    0    1   
$EndComp
$Comp
L R R6
U 1 1 5A1C7BDC
P 5200 3250
F 0 "R6" V 5280 3250 50  0000 C CNN
F 1 "10k" V 5200 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 5130 3250 50  0001 C CNN
F 3 "" H 5200 3250 50  0000 C CNN
	1    5200 3250
	0    -1   -1   0   
$EndComp
$Comp
L SW_PUSH SW2
U 1 1 5A1C7BED
P 4900 1500
F 0 "SW2" H 5050 1610 50  0000 C CNN
F 1 "SW_PUSH" H 4900 1420 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 4900 1500 50  0001 C CNN
F 3 "" H 4900 1500 50  0000 C CNN
	1    4900 1500
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5A1C7BF9
P 4050 2150
F 0 "R4" V 4130 2150 50  0000 C CNN
F 1 "10k" V 4050 2150 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3980 2150 50  0001 C CNN
F 3 "" H 4050 2150 50  0000 C CNN
	1    4050 2150
	0    1    1    0   
$EndComp
$Comp
L PWR_FLAG #FLG051
U 1 1 5A1C7C09
P 3350 2650
F 0 "#FLG051" H 3350 2745 50  0001 C CNN
F 1 "PWR_FLAG" H 3350 2830 50  0000 C CNN
F 2 "" H 3350 2650 50  0000 C CNN
F 3 "" H 3350 2650 50  0000 C CNN
	1    3350 2650
	-1   0    0    1   
$EndComp
$Comp
L C C16
U 1 1 5A1C7C12
P 4950 1850
F 0 "C16" V 4800 1800 50  0000 L CNN
F 1 "100nF" V 5100 1750 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4988 1700 50  0001 C CNN
F 3 "" H 4950 1850 50  0001 C CNN
	1    4950 1850
	0    1    1    0   
$EndComp
$Comp
L GND #PWR052
U 1 1 5A1C7C3C
P 5400 1900
F 0 "#PWR052" H 5400 1650 50  0001 C CNN
F 1 "GND" H 5400 1750 50  0000 C CNN
F 2 "" H 5400 1900 50  0001 C CNN
F 3 "" H 5400 1900 50  0001 C CNN
	1    5400 1900
	-1   0    0    1   
$EndComp
Text HLabel 7300 2300 2    60   Input ~ 0
TXD
Text HLabel 7300 2400 2    60   Input ~ 0
RXD
Text HLabel 3100 2650 0    60   Input ~ 0
VCC
Text Notes 10500 2950 2    60   ~ 0
Chave para iniciar o modo de programação
Connection ~ 3350 2650
Wire Wire Line
	5100 1850 5250 1850
Wire Wire Line
	4800 1850 4550 1850
Wire Wire Line
	4550 1500 4550 2150
$Comp
L SPDT SW4
U 1 1 5AC680E9
P 8250 3300
F 0 "SW4" H 8470 3400 40  0000 C CNN
F 1 "SPDT" H 8530 3230 40  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_Micro_SPST" H 8630 3300 60  0001 C CNN
F 3 "" H 8630 3300 60  0000 C CNN
	1    8250 3300
	1    0    0    -1  
$EndComp
NoConn ~ 8900 3200
$Comp
L GND #PWR053
U 1 1 5AC682A1
P 8900 3400
F 0 "#PWR053" H 8900 3150 50  0001 C CNN
F 1 "GND" H 8900 3250 50  0000 C CNN
F 2 "" H 8900 3400 50  0000 C CNN
F 3 "" H 8900 3400 50  0000 C CNN
	1    8900 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3100 2650 3900 2650
Connection ~ 3800 2650
Wire Wire Line
	6850 3800 6850 3700
Wire Wire Line
	6850 4150 6850 4100
Wire Wire Line
	3800 2050 3800 4350
$Comp
L R R27
U 1 1 5AC69FD9
P 7550 2900
F 0 "R27" V 7630 2900 50  0000 C CNN
F 1 "10K" V 7550 2900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 7480 2900 50  0001 C CNN
F 3 "" H 7550 2900 50  0000 C CNN
	1    7550 2900
	0    1    1    0   
$EndComp
$Comp
L R R28
U 1 1 5AC6B5FB
P 6050 3950
F 0 "R28" V 6130 3950 50  0000 C CNN
F 1 "330" V 6050 3950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 5980 3950 50  0001 C CNN
F 3 "" H 6050 3950 50  0000 C CNN
	1    6050 3950
	-1   0    0    1   
$EndComp
$Comp
L LED D3
U 1 1 5AC6B6D4
P 5700 4100
F 0 "D3" H 5700 4200 50  0000 C CNN
F 1 "LED" H 5700 4000 50  0000 C CNN
F 2 "LEDs:LED_0805" H 5700 4100 50  0001 C CNN
F 3 "" H 5700 4100 50  0000 C CNN
	1    5700 4100
	1    0    0    -1  
$EndComp
Text HLabel 5450 2350 0    60   Input ~ 0
ADC1_CH3
Text HLabel 7300 2200 2    60   Input ~ 0
SCL
Text HLabel 7300 2500 2    60   Input ~ 0
SDA
$Comp
L SW_PUSH SW5
U 1 1 5AC6C6B7
P 4500 2400
F 0 "SW5" H 4650 2510 50  0000 C CNN
F 1 "SW_PUSH" H 4500 2320 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 4500 2400 50  0001 C CNN
F 3 "" H 4500 2400 50  0000 C CNN
	1    4500 2400
	1    0    0    -1  
$EndComp
$Comp
L R R30
U 1 1 5AC6C71A
P 4050 2650
F 0 "R30" V 4130 2650 50  0000 C CNN
F 1 "10K" V 4050 2650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3980 2650 50  0001 C CNN
F 3 "" H 4050 2650 50  0000 C CNN
	1    4050 2650
	0    -1   -1   0   
$EndComp
$Comp
L C C35
U 1 1 5AC6C81C
P 4500 2650
F 0 "C35" H 4525 2750 50  0000 L CNN
F 1 "100nF" H 4525 2550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4538 2500 50  0001 C CNN
F 3 "" H 4500 2650 50  0000 C CNN
	1    4500 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 2150 5450 2150
Wire Wire Line
	4550 1500 4600 1500
Wire Wire Line
	5250 1500 5200 1500
Connection ~ 4550 1850
Connection ~ 4550 2150
Connection ~ 5250 1850
$Comp
L GND #PWR054
U 1 1 5AC6D165
P 4800 2700
F 0 "#PWR054" H 4800 2450 50  0001 C CNN
F 1 "GND" H 4800 2550 50  0000 C CNN
F 2 "" H 4800 2700 50  0000 C CNN
F 3 "" H 4800 2700 50  0000 C CNN
	1    4800 2700
	1    0    0    -1  
$EndComp
Text HLabel 5450 2450 0    60   Input ~ 0
ADC1_CH6
Text HLabel 5450 2550 0    60   Input ~ 0
ADC1_CH7
Text HLabel 5450 2250 0    60   Input ~ 0
ADC1_CH0
$Comp
L ESP-32S U3
U 1 1 5ACE63C4
P 6400 2650
F 0 "U3" H 5700 3900 60  0000 C CNN
F 1 "ESP-32S" H 6900 3900 60  0000 C CNN
F 2 "MPPT_Library:ESP32-WROOM" H 6750 4000 60  0001 C CNN
F 3 "" H 5950 3100 60  0001 C CNN
	1    6400 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 1500 5250 1950
Wire Wire Line
	5250 1950 5450 1950
Wire Wire Line
	5400 1900 5400 1950
Connection ~ 5400 1950
Wire Wire Line
	3800 2150 3900 2150
Wire Wire Line
	3800 2050 5450 2050
Connection ~ 3800 2150
$Comp
L R R14
U 1 1 5A1C7BB3
P 8150 3500
F 0 "R14" V 8230 3500 50  0000 C CNN
F 1 "10k" V 8150 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 8080 3500 50  0001 C CNN
F 3 "" H 8150 3500 50  0000 C CNN
	1    8150 3500
	-1   0    0    1   
$EndComp
Wire Wire Line
	7300 3300 8250 3300
Wire Wire Line
	8150 3350 8150 3300
Connection ~ 8150 3300
Wire Wire Line
	8150 4350 8150 3650
Wire Wire Line
	3800 4350 8150 4350
Wire Wire Line
	6750 3800 6750 3700
Wire Wire Line
	6750 4100 6750 4350
Connection ~ 6750 4350
$Comp
L GND #PWR055
U 1 1 5ACE4F54
P 6850 4150
F 0 "#PWR055" H 6850 3900 50  0001 C CNN
F 1 "GND" H 6850 4000 50  0000 C CNN
F 2 "" H 6850 4150 50  0000 C CNN
F 3 "" H 6850 4150 50  0000 C CNN
	1    6850 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3250 5350 3250
Wire Wire Line
	5050 3250 4950 3250
Wire Wire Line
	4950 3250 4950 3300
$Comp
L GND #PWR056
U 1 1 5ACE5324
P 4950 3300
F 0 "#PWR056" H 4950 3050 50  0001 C CNN
F 1 "GND" H 4950 3150 50  0000 C CNN
F 2 "" H 4950 3300 50  0000 C CNN
F 3 "" H 4950 3300 50  0000 C CNN
	1    4950 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2900 7400 2900
Wire Wire Line
	7700 2900 7700 4350
Connection ~ 7700 4350
Wire Wire Line
	6050 3700 6050 3800
Wire Wire Line
	6050 4100 5900 4100
Wire Wire Line
	5500 4100 5300 4100
Wire Wire Line
	5300 3700 5300 4150
$Comp
L GND #PWR057
U 1 1 5ACE5E74
P 5300 4150
F 0 "#PWR057" H 5300 3900 50  0001 C CNN
F 1 "GND" H 5300 4000 50  0000 C CNN
F 2 "" H 5300 4150 50  0000 C CNN
F 3 "" H 5300 4150 50  0000 C CNN
	1    5300 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3700 5300 3700
Connection ~ 5300 4100
$Comp
L GND #PWR058
U 1 1 5ACE5FC2
P 7550 2000
F 0 "#PWR058" H 7550 1750 50  0001 C CNN
F 1 "GND" H 7550 1850 50  0000 C CNN
F 2 "" H 7550 2000 50  0000 C CNN
F 3 "" H 7550 2000 50  0000 C CNN
	1    7550 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2000 7550 2000
Wire Wire Line
	7300 1900 7550 1900
Wire Wire Line
	7550 1900 7550 2000
Wire Wire Line
	4200 2400 4200 2950
Wire Wire Line
	4200 2650 4350 2650
Wire Wire Line
	4650 2650 4800 2650
Wire Wire Line
	4800 2400 4800 2700
Connection ~ 4800 2650
NoConn ~ 5450 3050
NoConn ~ 5450 3150
NoConn ~ 7300 3200
NoConn ~ 7300 3100
NoConn ~ 7300 3000
NoConn ~ 7300 2800
NoConn ~ 7300 2100
NoConn ~ 6150 3700
NoConn ~ 6250 3700
NoConn ~ 6350 3700
NoConn ~ 6450 3700
NoConn ~ 6550 3700
NoConn ~ 6650 3700
Text HLabel 7300 2700 2    60   Input ~ 0
NCO
Wire Wire Line
	5450 2650 4950 2650
Wire Wire Line
	4950 2650 4950 2950
Wire Wire Line
	4950 2950 4200 2950
Connection ~ 4200 2650
NoConn ~ 5450 2750
NoConn ~ 5450 2850
NoConn ~ 5450 2950
$EndSCHEMATC
