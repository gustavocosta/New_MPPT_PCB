EESchema Schematic File Version 2
LIBS:mppt_microchip-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:40xx
LIBS:74xgxx
LIBS:5050_ws2811
LIBS:a4988-module
LIBS:ac-dc
LIBS:acs712
LIBS:actel
LIBS:adxl345
LIBS:Altera
LIBS:AMS1117
LIBS:analog_devices
LIBS:analog_ICs_PL
LIBS:ant
LIBS:arduinoshield
LIBS:at24c64
LIBS:bc56-12EWA
LIBS:brooktre
LIBS:cis
LIBS:cmos_ieee
LIBS:con-kycon
LIBS:cp2103
LIBS:CP2102
LIBS:dc-dc
LIBS:dht11
LIBS:diode
LIBS:displays_PL
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:esp8266-module
LIBS:ESP8266
LIBS:fe
LIBS:ftdi
LIBS:Gajda_opto
LIBS:gennum
LIBS:graphic
LIBS:GS6300
LIBS:gy-a4988_module
LIBS:hc11
LIBS:hc541
LIBS:hc-05
LIBS:HC164
LIBS:hlk-rm04
LIBS:ir
LIBS:irf7907
LIBS:isp_6p
LIBS:kicad-library-by-simlun
LIBS:l298n
LIBS:Lattice
LIBS:LM2575
LIBS:logo
LIBS:logo-azura-wrench
LIBS:M25P05
LIBS:max1538
LIBS:maxim
LIBS:mcp1640
LIBS:mcp1640b
LIBS:mcp4921
LIBS:MCP3208
LIBS:microchip1
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers_PL
LIBS:Mimas-cache
LIBS:Mimas-rescue
LIBS:miniusb
LIBS:motor_drivers
LIBS:mp2109dq
LIBS:MP2307DN
LIBS:mppt-cache
LIBS:msp430
LIBS:na10-psu
LIBS:noname-cache
LIBS:nordicsemi
LIBS:nrf24l01
LIBS:numato_kicad_lib
LIBS:nxp_armmcu
LIBS:OF5032
LIBS:onsemi
LIBS:opamps
LIBS:opamps_PL
LIBS:Oscillators
LIBS:pl2303
LIBS:PL_capacitors
LIBS:PL_connectors
LIBS:PL_diodes
LIBS:PL_inductors
LIBS:PL_LEDs
LIBS:PL_mounting_holes
LIBS:PL_opto
LIBS:PL_switchers
LIBS:PL_switches
LIBS:PL_transistors
LIBS:pogopin
LIBS:Pot
LIBS:powerint
LIBS:powerjack
LIBS:Power_Management
LIBS:pspice
LIBS:pt4115
LIBS:references
LIBS:references_PL
LIBS:regulators_PL
LIBS:relays
LIBS:relays_PL
LIBS:rfcom
LIBS:RJ45-ENCODER
LIBS:rtl8196
LIBS:sensors
LIBS:silabs
LIBS:songle_relay
LIBS:special
LIBS:spiflash_16m
LIBS:st1s10
LIBS:stc12c5a60s2
LIBS:stepper_drivers
LIBS:stm8
LIBS:stm32
LIBS:STM32F100RBT6B
LIBS:stpdn
LIBS:supertex
LIBS:switches
LIBS:switch-omron
LIBS:SymbolsSimilarEN60617+oldDIN617
LIBS:tcs3200d
LIBS:tda5144
LIBS:texas-msp
LIBS:tp4056
LIBS:transf
LIBS:ts812c
LIBS:ttl_ieee
LIBS:usb_a
LIBS:usb-a-pcb
LIBS:usb_ax2
LIBS:video
LIBS:vref_PL
LIBS:w_analog
LIBS:w_connectors
LIBS:w_device
LIBS:w_logic
LIBS:Worldsemi
LIBS:xc3s100etq144
LIBS:xc3s500evq100
LIBS:xc6slx9-tqg144
LIBS:XC9572-TQ100
LIBS:Xicor
LIBS:xilinx_spartan3_virtex4_and_5
LIBS:Zilog
LIBS:рэс-49
LIBS:тпг-1
LIBS:KICAD-OPEN-LIBS
LIBS:4000-ic
LIBS:7400-ic
LIBS:analog-ic
LIBS:avr-mcu
LIBS:bluegiga
LIBS:connector
LIBS:diode-inc-ic
LIBS:freescale-ic
LIBS:ftdi-ic
LIBS:led
LIBS:maxim-ic
LIBS:micrel-ic
LIBS:microchip-ic
LIBS:nxp-ic
LIBS:on-semi-ic
LIBS:regulator
LIBS:rohm
LIBS:sharp-relay
LIBS:sparkfun
LIBS:standard
LIBS:stmicro-mcu
LIBS:ti-ic
LIBS:transistor
LIBS:uln-ic
LIBS:74LVC1G04GW
LIBS:93c56
LIBS:93CXX
LIBS:amesser-ad
LIBS:amesser-cmos4000
LIBS:amesser-conn
LIBS:amesser-discrete
LIBS:amesser-linear
LIBS:amesser-power
LIBS:amesser-usb
LIBS:audio-vlsi
LIBS:bat54_c
LIBS:bav199
LIBS:BLM15HG6015N1D
LIBS:bss138dp
LIBS:CIAA_ACC
LIBS:CIAAK60LIB
LIBS:DDR3_x16
LIBS:edu-ciaa-fsl
LIBS:edu-ciaa-nxp
LIBS:EduCiaaXSchLibrary
LIBS:FMC_HPC
LIBS:fqt13n06l
LIBS:ft232bm
LIBS:ft232rq
LIBS:ft2232h
LIBS:FT2232H
LIBS:g5v2
LIBS:intel_edison
LIBS:IS42S16400F
LIBS:lan_8740
LIBS:led_arbg
LIBS:lm2596
LIBS:lpc54102j512bd64
LIBS:LPC4337JBD144
LIBS:mcp1415
LIBS:mcp1416
LIBS:mcu-nxp
LIBS:mcu-st
LIBS:mic2039eymt
LIBS:MIC2025
LIBS:microusb
LIBS:net-phy
LIBS:nsi50010yt1g
LIBS:pbss5240xf
LIBS:PBSS5240
LIBS:pcie-mini
LIBS:Pic32MZ
LIBS:prtr5v0u2x
LIBS:PRTR5V0U2X
LIBS:PS2805
LIBS:regulators
LIBS:rs485_rs232_can
LIBS:r_small
LIBS:rx63n_lqfp144
LIBS:s25fl032p0xmfi013
LIBS:SCHA5B0200
LIBS:sd-con
LIBS:shield_arduino
LIBS:Si50x
LIBS:SMD_Sealing_Glass_Crystal
LIBS:sw_push
LIBS:sw_push4
LIBS:tblocks
LIBS:TJA1040
LIBS:TPS2051B
LIBS:transf_unip
LIBS:Transil_diode
LIBS:tvs
LIBS:TXB0108
LIBS:txs0206-29
LIBS:XC7Z030_FBG676
LIBS:XilinxArtix7
LIBS:ESP3212
LIBS:fm_radio
LIBS:ESP32-footprints-Shem-Lib
LIBS:mppt_microchip-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3400 1750 0    60   Input ~ 0
VCC
$Comp
L GND #PWR069
U 1 1 5AC682A1
P 6000 2350
F 0 "#PWR069" H 6000 2100 50  0001 C CNN
F 1 "GND" H 6000 2200 50  0000 C CNN
F 2 "" H 6000 2350 50  0000 C CNN
F 3 "" H 6000 2350 50  0000 C CNN
	1    6000 2350
	0    -1   -1   0   
$EndComp
$Comp
L R R25
U 1 1 5AC6B5FB
P 4800 3150
F 0 "R25" V 4700 3150 50  0000 C CNN
F 1 "330" V 4800 3150 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 4730 3150 50  0001 C CNN
F 3 "" H 4800 3150 50  0000 C CNN
	1    4800 3150
	0    -1   -1   0   
$EndComp
$Comp
L LED D3
U 1 1 5AC6B6D4
P 4300 3150
F 0 "D3" H 4300 3250 50  0000 C CNN
F 1 "LED" H 4300 3050 50  0000 C CNN
F 2 "LEDs:LED_0805" H 4300 3150 50  0001 C CNN
F 3 "" H 4300 3150 50  0000 C CNN
	1    4300 3150
	1    0    0    -1  
$EndComp
Text HLabel 5050 2050 0    60   Input ~ 0
ADC1_CH3
Text HLabel 5900 1950 2    60   Input ~ 0
SCL
Text HLabel 5900 2250 2    60   Input ~ 0
SDA
$Comp
L SW_PUSH SW2
U 1 1 5AC6C6B7
P 4100 2100
F 0 "SW2" H 4250 2210 50  0000 C CNN
F 1 "SW_PUSH" H 4100 2020 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 4100 2100 50  0001 C CNN
F 3 "" H 4100 2100 50  0000 C CNN
	1    4100 2100
	1    0    0    -1  
$EndComp
$Comp
L R R24
U 1 1 5AC6C71A
P 3650 2350
F 0 "R24" V 3730 2350 50  0000 C CNN
F 1 "10K" V 3650 2350 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3580 2350 50  0001 C CNN
F 3 "" H 3650 2350 50  0000 C CNN
	1    3650 2350
	0    -1   -1   0   
$EndComp
$Comp
L C C35
U 1 1 5AC6C81C
P 4100 2350
F 0 "C35" H 4125 2450 50  0000 L CNN
F 1 "100nF" H 4125 2250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4138 2200 50  0001 C CNN
F 3 "" H 4100 2350 50  0000 C CNN
	1    4100 2350
	0    1    1    0   
$EndComp
$Comp
L GND #PWR070
U 1 1 5AC6D165
P 4400 2400
F 0 "#PWR070" H 4400 2150 50  0001 C CNN
F 1 "GND" H 4400 2250 50  0000 C CNN
F 2 "" H 4400 2400 50  0000 C CNN
F 3 "" H 4400 2400 50  0000 C CNN
	1    4400 2400
	1    0    0    -1  
$EndComp
Text HLabel 5050 2150 0    60   Input ~ 0
ADC1_CH6
Text HLabel 5050 2250 0    60   Input ~ 0
ADC1_CH7
Text HLabel 5050 1950 0    60   Input ~ 0
ADC1_CH0
$Comp
L GND #PWR071
U 1 1 5ACE4F54
P 4800 3050
F 0 "#PWR071" H 4800 2800 50  0001 C CNN
F 1 "GND" H 4800 2900 50  0000 C CNN
F 2 "" H 4800 3050 50  0000 C CNN
F 3 "" H 4800 3050 50  0000 C CNN
	1    4800 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	4650 3150 4500 3150
Wire Wire Line
	4100 3150 3900 3150
$Comp
L GND #PWR072
U 1 1 5ACE5E74
P 3900 3150
F 0 "#PWR072" H 3900 2900 50  0001 C CNN
F 1 "GND" H 3900 3000 50  0000 C CNN
F 2 "" H 3900 3150 50  0000 C CNN
F 3 "" H 3900 3150 50  0000 C CNN
	1    3900 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR073
U 1 1 5ACE5FC2
P 6000 1750
F 0 "#PWR073" H 6000 1500 50  0001 C CNN
F 1 "GND" H 6000 1600 50  0000 C CNN
F 2 "" H 6000 1750 50  0000 C CNN
F 3 "" H 6000 1750 50  0000 C CNN
	1    6000 1750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3800 2100 3800 2650
Wire Wire Line
	3800 2350 3950 2350
Wire Wire Line
	4250 2350 4400 2350
Wire Wire Line
	4400 2100 4400 2400
Connection ~ 4400 2350
Wire Wire Line
	5050 2350 4550 2350
Wire Wire Line
	4550 2350 4550 2650
Wire Wire Line
	4550 2650 3800 2650
Connection ~ 3800 2350
$Comp
L CONN_01X19 P9
U 1 1 5B43AA22
P 5250 2650
F 0 "P9" H 5250 3650 50  0000 C CNN
F 1 "CONN_01X19" V 5350 2650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x19_Pitch2.54mm" H 5250 2650 50  0001 C CNN
F 3 "" H 5250 2650 50  0000 C CNN
	1    5250 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1750 5050 1750
Wire Wire Line
	3500 2350 3500 1750
Connection ~ 3500 1750
NoConn ~ 5050 1850
$Comp
L CONN_01X19 P10
U 1 1 5B43B219
P 5700 2650
F 0 "P10" H 5700 3650 50  0000 C CNN
F 1 "CONN_01X19" V 5800 2650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x19_Pitch2.54mm" H 5700 2650 50  0001 C CNN
F 3 "" H 5700 2650 50  0000 C CNN
	1    5700 2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	4800 3050 5050 3050
Wire Wire Line
	4950 3150 5050 3150
Wire Wire Line
	5900 1750 6000 1750
Wire Wire Line
	5900 2350 6000 2350
NoConn ~ 5050 2450
NoConn ~ 5050 2550
NoConn ~ 5050 2650
NoConn ~ 5050 2750
NoConn ~ 5050 2850
NoConn ~ 5050 2950
NoConn ~ 5050 3250
NoConn ~ 5050 3350
NoConn ~ 5050 3450
NoConn ~ 5050 3550
NoConn ~ 5900 1850
NoConn ~ 5900 2050
NoConn ~ 5900 2150
NoConn ~ 5900 2450
NoConn ~ 5900 2550
NoConn ~ 5900 2650
NoConn ~ 5900 2750
NoConn ~ 5900 2850
NoConn ~ 5900 2950
NoConn ~ 5900 3050
NoConn ~ 5900 3150
NoConn ~ 5900 3250
NoConn ~ 5900 3350
NoConn ~ 5900 3450
NoConn ~ 5900 3550
$EndSCHEMATC
