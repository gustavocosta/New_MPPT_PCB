EESchema Schematic File Version 2
LIBS:mppt_microchip-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:40xx
LIBS:74xgxx
LIBS:5050_ws2811
LIBS:a4988-module
LIBS:ac-dc
LIBS:acs712
LIBS:actel
LIBS:adxl345
LIBS:Altera
LIBS:AMS1117
LIBS:analog_devices
LIBS:analog_ICs_PL
LIBS:ant
LIBS:arduinoshield
LIBS:at24c64
LIBS:bc56-12EWA
LIBS:brooktre
LIBS:cis
LIBS:cmos_ieee
LIBS:con-kycon
LIBS:cp2103
LIBS:CP2102
LIBS:dc-dc
LIBS:dht11
LIBS:diode
LIBS:displays_PL
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:esp8266-module
LIBS:ESP8266
LIBS:fe
LIBS:ftdi
LIBS:Gajda_opto
LIBS:gennum
LIBS:graphic
LIBS:GS6300
LIBS:gy-a4988_module
LIBS:hc11
LIBS:hc541
LIBS:hc-05
LIBS:HC164
LIBS:hlk-rm04
LIBS:ir
LIBS:irf7907
LIBS:isp_6p
LIBS:kicad-library-by-simlun
LIBS:l298n
LIBS:Lattice
LIBS:LM2575
LIBS:logo
LIBS:logo-azura-wrench
LIBS:M25P05
LIBS:max1538
LIBS:maxim
LIBS:mcp1640
LIBS:mcp1640b
LIBS:mcp4921
LIBS:MCP3208
LIBS:microchip1
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers_PL
LIBS:Mimas-cache
LIBS:Mimas-rescue
LIBS:miniusb
LIBS:motor_drivers
LIBS:mp2109dq
LIBS:MP2307DN
LIBS:mppt-cache
LIBS:msp430
LIBS:na10-psu
LIBS:noname-cache
LIBS:nordicsemi
LIBS:nrf24l01
LIBS:numato_kicad_lib
LIBS:nxp_armmcu
LIBS:OF5032
LIBS:onsemi
LIBS:opamps
LIBS:opamps_PL
LIBS:Oscillators
LIBS:pl2303
LIBS:PL_capacitors
LIBS:PL_connectors
LIBS:PL_diodes
LIBS:PL_inductors
LIBS:PL_LEDs
LIBS:PL_mounting_holes
LIBS:PL_opto
LIBS:PL_switchers
LIBS:PL_switches
LIBS:PL_transistors
LIBS:pogopin
LIBS:Pot
LIBS:powerint
LIBS:powerjack
LIBS:Power_Management
LIBS:pspice
LIBS:pt4115
LIBS:references
LIBS:references_PL
LIBS:regulators_PL
LIBS:relays
LIBS:relays_PL
LIBS:rfcom
LIBS:RJ45-ENCODER
LIBS:rtl8196
LIBS:sensors
LIBS:silabs
LIBS:songle_relay
LIBS:special
LIBS:spiflash_16m
LIBS:st1s10
LIBS:stc12c5a60s2
LIBS:stepper_drivers
LIBS:stm8
LIBS:stm32
LIBS:STM32F100RBT6B
LIBS:stpdn
LIBS:supertex
LIBS:switches
LIBS:switch-omron
LIBS:SymbolsSimilarEN60617+oldDIN617
LIBS:tcs3200d
LIBS:tda5144
LIBS:texas-msp
LIBS:tp4056
LIBS:transf
LIBS:ts812c
LIBS:ttl_ieee
LIBS:usb_a
LIBS:usb-a-pcb
LIBS:usb_ax2
LIBS:video
LIBS:vref_PL
LIBS:w_analog
LIBS:w_connectors
LIBS:w_device
LIBS:w_logic
LIBS:Worldsemi
LIBS:xc3s100etq144
LIBS:xc3s500evq100
LIBS:xc6slx9-tqg144
LIBS:XC9572-TQ100
LIBS:Xicor
LIBS:xilinx_spartan3_virtex4_and_5
LIBS:Zilog
LIBS:рэс-49
LIBS:тпг-1
LIBS:KICAD-OPEN-LIBS
LIBS:4000-ic
LIBS:7400-ic
LIBS:analog-ic
LIBS:avr-mcu
LIBS:bluegiga
LIBS:connector
LIBS:diode-inc-ic
LIBS:freescale-ic
LIBS:ftdi-ic
LIBS:led
LIBS:maxim-ic
LIBS:micrel-ic
LIBS:microchip-ic
LIBS:nxp-ic
LIBS:on-semi-ic
LIBS:regulator
LIBS:rohm
LIBS:sharp-relay
LIBS:sparkfun
LIBS:standard
LIBS:stmicro-mcu
LIBS:ti-ic
LIBS:transistor
LIBS:uln-ic
LIBS:74LVC1G04GW
LIBS:93c56
LIBS:93CXX
LIBS:amesser-ad
LIBS:amesser-cmos4000
LIBS:amesser-conn
LIBS:amesser-discrete
LIBS:amesser-linear
LIBS:amesser-power
LIBS:amesser-usb
LIBS:audio-vlsi
LIBS:bat54_c
LIBS:bav199
LIBS:BLM15HG6015N1D
LIBS:bss138dp
LIBS:CIAA_ACC
LIBS:CIAAK60LIB
LIBS:DDR3_x16
LIBS:edu-ciaa-fsl
LIBS:edu-ciaa-nxp
LIBS:EduCiaaXSchLibrary
LIBS:FMC_HPC
LIBS:fqt13n06l
LIBS:ft232bm
LIBS:ft232rq
LIBS:ft2232h
LIBS:FT2232H
LIBS:g5v2
LIBS:intel_edison
LIBS:IS42S16400F
LIBS:lan_8740
LIBS:led_arbg
LIBS:lm2596
LIBS:lpc54102j512bd64
LIBS:LPC4337JBD144
LIBS:mcp1415
LIBS:mcp1416
LIBS:mcu-nxp
LIBS:mcu-st
LIBS:mic2039eymt
LIBS:MIC2025
LIBS:microusb
LIBS:net-phy
LIBS:nsi50010yt1g
LIBS:pbss5240xf
LIBS:PBSS5240
LIBS:pcie-mini
LIBS:Pic32MZ
LIBS:prtr5v0u2x
LIBS:PRTR5V0U2X
LIBS:PS2805
LIBS:regulators
LIBS:rs485_rs232_can
LIBS:r_small
LIBS:rx63n_lqfp144
LIBS:s25fl032p0xmfi013
LIBS:SCHA5B0200
LIBS:sd-con
LIBS:shield_arduino
LIBS:Si50x
LIBS:SMD_Sealing_Glass_Crystal
LIBS:sw_push
LIBS:sw_push4
LIBS:tblocks
LIBS:TJA1040
LIBS:TPS2051B
LIBS:transf_unip
LIBS:Transil_diode
LIBS:tvs
LIBS:TXB0108
LIBS:txs0206-29
LIBS:XC7Z030_FBG676
LIBS:XilinxArtix7
LIBS:ESP3212
LIBS:fm_radio
LIBS:ESP32-footprints-Shem-Lib
LIBS:mppt_microchip-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AD7829-1 U5
U 1 1 5A1D2750
P 7700 4000
F 0 "U5" H 5750 3700 60  0000 C CNN
F 1 "AD7829-1" H 5750 4550 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-28_4.4x9.7mm_Pitch0.65mm" H 5700 5400 60  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/AD7829-1.pdf" H 5750 5550 60  0001 C CNN
	1    7700 4000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR036
U 1 1 5A1D2780
P 4850 3400
F 0 "#PWR036" H 4850 3150 50  0001 C CNN
F 1 "GND" H 4850 3250 50  0000 C CNN
F 2 "" H 4850 3400 50  0001 C CNN
F 3 "" H 4850 3400 50  0001 C CNN
	1    4850 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 3400 4850 3400
$Comp
L GND #PWR037
U 1 1 5A1D2787
P 6600 3300
F 0 "#PWR037" H 6600 3050 50  0001 C CNN
F 1 "GND" H 6600 3150 50  0000 C CNN
F 2 "" H 6600 3300 50  0001 C CNN
F 3 "" H 6600 3300 50  0001 C CNN
	1    6600 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6350 3300 6600 3300
Wire Wire Line
	6650 3500 6350 3500
$Comp
L +3.3V #PWR038
U 1 1 5A1D2793
P 6800 3400
F 0 "#PWR038" H 6800 3250 50  0001 C CNN
F 1 "+3.3V" H 6800 3540 50  0000 C CNN
F 2 "" H 6800 3400 50  0001 C CNN
F 3 "" H 6800 3400 50  0001 C CNN
	1    6800 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 3400 6800 3400
$Comp
L +2V5 #PWR039
U 1 1 5A1D279A
P 6650 3500
F 0 "#PWR039" H 6650 3350 50  0001 C CNN
F 1 "+2V5" H 6650 3640 50  0000 C CNN
F 2 "" H 6650 3500 50  0001 C CNN
F 3 "" H 6650 3500 50  0001 C CNN
	1    6650 3500
	0    1    1    0   
$EndComp
Text HLabel 5100 3000 0    60   Input ~ 0
DB0
Text HLabel 5100 2900 0    60   Input ~ 0
DB1
Text HLabel 5100 2800 0    60   Input ~ 0
DB2
Text HLabel 6350 2800 2    60   Input ~ 0
DB3
Text HLabel 6350 2900 2    60   Input ~ 0
DB4
Text HLabel 6350 3000 2    60   Input ~ 0
DB5
Text HLabel 6350 3100 2    60   Input ~ 0
DB6
Text HLabel 6350 3200 2    60   Input ~ 0
DB7
Text HLabel 5100 3100 0    60   Input ~ 0
CONVST
Text HLabel 5100 3200 0    60   Input ~ 0
CS
Text HLabel 5100 3300 0    60   Input ~ 0
RD
Text HLabel 5100 3500 0    60   Input ~ 0
EOC
Text HLabel 5100 3600 0    60   Input ~ 0
A2
Text HLabel 5100 3700 0    60   Input ~ 0
A1
Text HLabel 5100 3800 0    60   Input ~ 0
A0
Text HLabel 6350 3700 2    60   Input ~ 0
AD_IN1
Text HLabel 6350 3800 2    60   Input ~ 0
AD_IN2
Text HLabel 6350 3900 2    60   Input ~ 0
AD_IN3
Text HLabel 6350 4000 2    60   Input ~ 0
AD_IN4
Text HLabel 5100 3900 0    60   Input ~ 0
VSENSE_IN
Text HLabel 5100 4000 0    60   Input ~ 0
VSENSE_OUT
Text HLabel 6350 4100 2    60   Input ~ 0
ISENSE_IN
Text HLabel 5100 4100 0    60   Input ~ 0
PCG/ISENSE_OUT
Text Notes 7050 3550 0    60   ~ 0
2.5V é a voltagem necessária para fixar o alcance do ADC em 0 até 2,0V
NoConn ~ 6350 3600
$EndSCHEMATC
