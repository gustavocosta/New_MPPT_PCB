EESchema Schematic File Version 2
LIBS:PL_diodes
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:example-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "11 apr 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L DIODE D?
U 1 1 55294F84
P 5700 4150
F 0 "D?" H 5700 4250 40  0000 C CNN
F 1 "DIODE" H 5700 4050 40  0000 C CNN
F 2 "~" H 5700 4150 60  0000 C CNN
F 3 "~" H 5700 4150 60  0000 C CNN
	1    5700 4150
	1    0    0    -1  
$EndComp
$Comp
L SCHOTTKY D?
U 1 1 55294F93
P 5700 4500
F 0 "D?" H 5700 4600 40  0000 C CNN
F 1 "SCHOTTKY" H 5700 4400 40  0000 C CNN
F 2 "~" H 5700 4500 60  0000 C CNN
F 3 "~" H 5700 4500 60  0000 C CNN
	1    5700 4500
	1    0    0    -1  
$EndComp
$Comp
L ZENER D?
U 1 1 55294FA2
P 5700 4850
F 0 "D?" H 5700 4950 40  0000 C CNN
F 1 "ZENER" H 5700 4750 40  0000 C CNN
F 2 "~" H 5700 4850 60  0000 C CNN
F 3 "~" H 5700 4850 60  0000 C CNN
	1    5700 4850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
