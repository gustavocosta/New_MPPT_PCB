EESchema Schematic File Version 4
LIBS:mppt_microchip-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L adc-dac:AD7829-1 U5
U 1 1 5A1D2750
P 7700 4000
F 0 "U5" H 5750 3700 60  0000 C CNN
F 1 "AD7829-1" H 5750 4550 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-28_4.4x9.7mm_Pitch0.65mm" H 5700 5400 60  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/AD7829-1.pdf" H 5750 5550 60  0001 C CNN
	1    7700 4000
	1    0    0    -1  
$EndComp
$Comp
L power1:GND #PWR036
U 1 1 5A1D2780
P 4850 3400
F 0 "#PWR036" H 4850 3150 50  0001 C CNN
F 1 "GND" H 4850 3250 50  0000 C CNN
F 2 "" H 4850 3400 50  0001 C CNN
F 3 "" H 4850 3400 50  0001 C CNN
	1    4850 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 3400 4850 3400
$Comp
L power1:GND #PWR037
U 1 1 5A1D2787
P 6600 3300
F 0 "#PWR037" H 6600 3050 50  0001 C CNN
F 1 "GND" H 6600 3150 50  0000 C CNN
F 2 "" H 6600 3300 50  0001 C CNN
F 3 "" H 6600 3300 50  0001 C CNN
	1    6600 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6350 3300 6600 3300
Wire Wire Line
	6650 3500 6350 3500
$Comp
L power1:+3.3V #PWR038
U 1 1 5A1D2793
P 6800 3400
F 0 "#PWR038" H 6800 3250 50  0001 C CNN
F 1 "+3.3V" H 6800 3540 50  0000 C CNN
F 2 "" H 6800 3400 50  0001 C CNN
F 3 "" H 6800 3400 50  0001 C CNN
	1    6800 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 3400 6800 3400
$Comp
L power1:+2V5 #PWR039
U 1 1 5A1D279A
P 6650 3500
F 0 "#PWR039" H 6650 3350 50  0001 C CNN
F 1 "+2V5" H 6650 3640 50  0000 C CNN
F 2 "" H 6650 3500 50  0001 C CNN
F 3 "" H 6650 3500 50  0001 C CNN
	1    6650 3500
	0    1    1    0   
$EndComp
Text HLabel 5100 3000 0    60   Input ~ 0
DB0
Text HLabel 5100 2900 0    60   Input ~ 0
DB1
Text HLabel 5100 2800 0    60   Input ~ 0
DB2
Text HLabel 6350 2800 2    60   Input ~ 0
DB3
Text HLabel 6350 2900 2    60   Input ~ 0
DB4
Text HLabel 6350 3000 2    60   Input ~ 0
DB5
Text HLabel 6350 3100 2    60   Input ~ 0
DB6
Text HLabel 6350 3200 2    60   Input ~ 0
DB7
Text HLabel 5100 3100 0    60   Input ~ 0
CONVST
Text HLabel 5100 3200 0    60   Input ~ 0
CS
Text HLabel 5100 3300 0    60   Input ~ 0
RD
Text HLabel 5100 3500 0    60   Input ~ 0
EOC
Text HLabel 5100 3600 0    60   Input ~ 0
A2
Text HLabel 5100 3700 0    60   Input ~ 0
A1
Text HLabel 5100 3800 0    60   Input ~ 0
A0
Text HLabel 6350 3700 2    60   Input ~ 0
AD_IN1
Text HLabel 6350 3800 2    60   Input ~ 0
AD_IN2
Text HLabel 6350 3900 2    60   Input ~ 0
AD_IN3
Text HLabel 6350 4000 2    60   Input ~ 0
AD_IN4
Text HLabel 5100 3900 0    60   Input ~ 0
VSENSE_IN
Text HLabel 5100 4000 0    60   Input ~ 0
VSENSE_OUT
Text HLabel 6350 4100 2    60   Input ~ 0
ISENSE_IN
Text HLabel 5100 4100 0    60   Input ~ 0
PCG/ISENSE_OUT
Text Notes 7050 3550 0    60   ~ 0
2.5V é a voltagem necessária para fixar o alcance do ADC em 0 até 2,0V
NoConn ~ 6350 3600
$EndSCHEMATC
